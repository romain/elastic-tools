<?php
namespace Romain\Elastic;

/**
 * Better simple ES client with some bulk improvements
 *
 * Could be improved by auto detect default best index and type
 */
Class EsClientBulk extends EsClientSimple {

	/**
	 * Build a commande bulk line(s)
	 * @param string $command
	 * @param string $index
	 * @param string $type
	 * @param string $id
	 * @param mixed $data
	 * @return array
	 */
	protected function _bulkLine($command, $index, $type, $id, $data = null) {
		// if no default type or index, set as default
		if(empty($this->_bulk['index'])) {
			$this->_bulk['index'] = $index;
		}
		if(empty($this->_bulk['type'])) {
			$this->_bulk['type'] = $type;
			
		}

		// if index or type equal to default, unset it
		if(!empty($this->_bulk['index']) && $this->_bulk['index'] == $index) {
			$index = null;
		}
		if(!empty($this->_bulk['type']) && $this->_bulk['type'] == $type) {
			$type = null;
		}

		// build line and bulk
		$line = array($command => array('_id' => $id));
		if(!is_null($index)) {
			$line[$command]['_index'] = $index;
		}
		if(!is_null($index)) {
			$line[$command]['_type'] = $type;
		}

		$bulk = array($line);

		if($command === 'index') {
			if($data === null) {
				throw new \Exception('Bulk index need data');
			}
			$bulk[] = (array)$data;
		}
		return $bulk;
	}
}