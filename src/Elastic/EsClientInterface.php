<?php
namespace Romain\Elastic;

/**
 * Mini ES client over Elasticserch-php
 *
 * @TODO handle default bulk data (index, type... less data to send)
 */
interface EsClientInterface {

	/**
	 * Initialise client
	 * @param array $conf
	 */
	public function init($conf);

	/**
	 * Index an object
	 * @param string $index
	 * @param string $type
	 * @param string $id
	 * @param mixed $data
	 */
	public function index($index, $type, $id, $data);

	/**
	 * Clear the prepared bulk pull
	 */
	public function bulkClear($bulk = array());

	/**
	 * Prepare bulk index lines
	 * @param string $index
	 * @param string $type
	 * @param string $id
	 * @param mixed $data
	 */
	public function bulkAddIndex($index, $type, $id, $data);

	/**
	 * Prepare a bulk delete line
	 * @param string $index
	 * @param string $type
	 * @param string $id
	 */
	public function bulkAddDelete($index, $type, $id);

	/**
	 * Add lines to prepared bulk pull
	 * @param array $bulk
	 */
	public function bulkAdd($bulk);

	/**
	 * Send all prepared bullk to ES
	 */
	public function bulkPrepared();

	/**
	 * Send bulk to ES
	 * @param array $bulk
	 * @return array
	 */
	public function bulk($bulk);

	/**
	 * Delete an object
	 * @param string $index
	 * @param string $type
	 * @param string $id
	 */
	public function delete($index, $type, $id);

	/**
	 * Drop an index
	 * @param string $index
	 */
	public function dropIndex($index);

	/**
	 * Create an index
	 * @param string $index
	 * @param array $body
	 * @return type
	 */
	public function createIndex($index, $body = array());
	
}