<?php
namespace Romain\Elastic;

use Elasticsearch\Client;

/**
 * Simple ES client over Elasticserch-php
 *
 * bulk implementation is brute bulk
 *
 */
Class EsClientSimple implements EsClientInterface {

	/**
	 * Current client config
	 * @var array
	 */
	protected $_conf = array();

	/**
	 * ES client
	 * @var Client
	 */
	protected $_elastic = null;

	/**
	 * Prepared bulk
	 * @var array
	 */
	protected $_bulk = array();


	/**
	 *
	 * @param array $conf
	 */
	public function __construct($conf) {
		$this->init($conf);
	}

	/**
	 * Initialise client
	 * @param array $conf
	 */
	public function init($conf) {
		$conf = (array)$conf + array(
			'hosts' => array('http://localhost:9200')
		);
		$this->_conf = $conf;
		
		$this->bulkClear();
		$this->_elastic = new Client($this->_conf);
	}

	/**
	 * Index an object
	 * @param string $index
	 * @param string $type
	 * @param string $id
	 * @param mixed $data
	 */
	public function index($index, $type, $id, $data) {
		$params = array();
		$params['body'] = $data + array('_id' => $id);
		$params['index'] = $index;
		$params['type'] = $type;
		$params['id'] = $id;
		return $this->_elastic->index($params);
	}

	/**
	 * Clear the prepared bulk pull
	 */
	public function bulkClear($bulk = array()) {
		$this->_bulk = $bulk + array('body' => array());
	}

	/**
	 * Prepare bulk index lines
	 * @param string $index
	 * @param string $type
	 * @param string  $id
	 * @param mixed $data
	 */
	public function bulkAddIndex($index, $type, $id, $data) {
		$this->bulkAdd($this->_bulkLine('index', $index, $type, $id, $data));
	}

	/**
	 * Prepare a bulk delete line
	 * @param string $index
	 * @param string $type
	 * @param string $id
	 */
	public function bulkAddDelete($index, $type, $id) {
		$this->bulkAdd($this->_bulkLine('delete', $index, $type, $id));
	}

	/**
	 * Build a commande bulk line(s)
	 * @param string $command
	 * @param string $index
	 * @param string $type
	 * @param string $id
	 * @param mixed $data
	 * @return array
	 */
	protected function _bulkLine($command, $index, $type, $id, $data = null) {
		$bulk = array(
			array($command => array('_index' => $index, '_type' => $type, '_id' => $id))
		);
		if($command === 'index') {
			if($data === null) {
				throw new \Exception('Bulk index need data');
			}
			$bulk[] = (array)$data;
		}
		return $bulk;
	}

	/**
	 * Add lines to prepared bulk pull
	 * @param array $bulk
	 */
	public function bulkAdd($bulk) {
		foreach($bulk as $line)  {
			$this->_bulk['body'][] = $line;
		}
	}

	/**
	 * Send all prepared bullk to ES
	 */
	public function bulkPrepared() {
		return $this->bulk($this->_bulk);
	}

	/**
	 * Send bulk to AS
	 * @param array $bulk
	 * @return array
	 */
	public function bulk($bulk) {
		return $this->_elastic->bulk($bulk);
	}

	/**
	 * Delete an object
	 * @param string $index
	 * @param string $type
	 * @param string $id
	 */
	public function delete($index, $type, $id) {
		$params = array();
		$params['index'] = $index;
		$params['type'] = $type;
		$params['id'] = $id;
		$this->_elastic->delete($params);
	}

	/**
	 * Drop an index
	 * @param string $index
	 */
	public function dropIndex($index) {
		$params = array(
			'index' => $index
		);
		return $this->_elastic->indices()->delete($params);
	}

	/**
	 * Create an index
	 * @param string $index
	 * @param string $body
	 * @return string
	 */
	public function createIndex($index, $body = array()) {
		$params = array();
		$params['index']  = $index;
		$params['body'] = $body;
		return $this->_elastic->indices()->create($params);
	}
	
}